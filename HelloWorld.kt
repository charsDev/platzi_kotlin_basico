fun main (args: Array<String>) {  
    println("Hola Platzi")
   
    // Operadores con tipo de datos primitivos
    println(1+1)
    println(3-1)
    println(2*2)
    println(4/2)
    println(7%2)
   
    // Tratando valores primitvos como objetos
    val a = 4
    val b = 2

    println(a.plus(b))
    println(a.minus(b))
    println(a.times(b))
    println(a.div(b))

    // Ejercicio var val const
    var n = 4
    println(n)

    val name = "" //tiempo de ejecucion

    // Apartir de aqui ya todo el codigo sera mediante IntelliJ.
    // Solo demostre que si podia hacelo mnediante vscode.
}