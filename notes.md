# Por que debo aprender Kotlin

* __Kotlin__ creado por __*JetBrains*__ lo decidieron hacer basado en la máquina virtual de Java, es decir puedes trabajar con Java y Kotlin en una aplicación ya que comparten la misma máquina virtual.
* __Es versatil__ podemos aplicarlo en diferentes tipos de aplicaciones. Del lado del servidor con Kotlin Server Side - Ktor, del lado Mobile Android y del lado web con KotlinJS.
* __Kotlin y Java__ sus compiladores (Kotlinc y Javac) nos generan codigo __*Bytecode*__ que es el código que lee la máquina virtual de Java (Java Virtual Machine). 
* __Kotlin/Native__ es una tecnologia que se usa para compilar codigo binario sin depender de una maquina virtual.

# Mi primer programa en Kotlin

## Instalacion 

* Primero instale [JAVA SDK 12](https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html). Descargue el .exe para que sea mas facil la instalacion.

* Despues instale [Kotlin Compiler](https://kotlinlang.org/docs/tutorials/command-line.html). Este archivo hay que descomprimirlo y copiar la carpeta __*Kotlinc*__ a la carpeta __Archivos de Programas__

* Hay que declarar estos programas en el path de las variables de entorno.

* En el caso de VSCode tenemos que instalar la extension __Code Runner__ y __Kotlin Language__

* Para ver el Output del codigo, damos click derecho sobre el archivo .kt y utilizamos la opcion __Run Code__ que despliega una pantalla extra donde muestra la consola.

* *En el momento de hacer estas notas, en la consola se visualizaba varios Warnings. No se como quitarlos, al parecer son cuestiones de Java SDK*

* En el caso de IntelliJ es mas facil, ya que instalamos el IDE y como ya habiamos isntalado previamente el JDK nada mas es cuestion de crear el proyecto con el nombre que queramos, la aplicación creara carpetas y archivos necesarios y nosotros solo nos dedicamos a programar.

## Primer Programa HelloWorld.kt

* La funcion __main__ es el punto de entrada de la aplicación.

* Se pueden pasar paramteros __args__ que sera igual a un __*Arreglo de tipo String*__ que se podra utilizar en el interior de la función.

* __Println__ imprime texto, numeros, etc en la pantalla de la consola.

# Conceptos Basicos
 
## Variables VS Objetos

* __Una varible__ es un espacio en memoria que reservamos para almacenar un solo dato.

* __Un objeto__ es un espacio en memoria mas complejo que una variable, ya que este puede contener:
  * Variables, Objetos, Acciones, Metodos, Funcionalidades.

* En Kotlin todo es un __objeto__ o al menos vamos a tratar de que todos los datos sean tratados como un objeto.

* Trataremos de evitar los tipos de datos que sean __variables__, __simples__ o __sencillos__ tambien conocidos como __tipos de datos primitivos__

* __Wrappers__ Son clases ya definidas dentro de la API del propio lenguaje de programacion, que añade funcionalidades a los datos primitivos. Ejemplo:
  * Si quisieramos sumar a = 2 + b = 2 utilizariamos el metodo de SUM de los Integers quedando algo asi __a.plus(b)__. *Eso entendi*

## Operadores en Kotlin

* En esta lección mediante ejemplos entendi como es que Kotlin no se deben usar datos primitvos ya que estos no tienen funcionalidaes ni metodos los cuales puedan ser usados para una mejor programación. Y tambien aprendimos como tratar estos datos primitivos a Objetos.

## Otros operadores en Kotlin

* Cuando usamos un operador en Kotlin él compilador lo traduce a una función específica.

* A continuacion link de alguno de los operadores mas usados segun el cruso[Otros Operadores en Kotlin](https://platzi.com/clases/1543-kotlin/19399-otros-operadores-en-kotlin/)

## Tipos de variables: var, val y const

* Hay dos tipos de variables en Kotlin:
  * Que pueden cambiar su valor(changeables) como __var__
  * Que no cambian su valor(unchangeables) __val__ y __const__ 

* Diferencia entre __const__ y __val__:
  * const: El valor se determina en el tiempo de compilacion.
  * val: el valor se puede determinar en tiempo de ejecucion.

* En kotlin preferimos las variables que no cambian su valor(unchangeables), esto por que se sigue los principios de la programacion funcional y uno de ellos es la __inmutabilidad__

## Ejercicio var, val y const

* A kotlin no le gustan las variables tipo __var__, ya que estas pueden cambiar su valor. Evitaremos utilizar varibales de este tipo.

### Funciones Puras
* Nada debe alterar el resultado de estas funciones, sobre todo con un valor externo. Por esos las variables changeables(var) no le gustan a Kotlin.

* __Val__ es mas utilizada en *variables locales* que esten declaradas en funciones(Metodos).
* __Const__ se utiliza para declarar *variables globales*, fuera de la funcion.

* En __*IntelliJ*__  en la variable __val name__ el valor de args lo obtuvimos de los argumentos que se le pasan a la __fun main__ como parametros. Para poder pasar estos parametros realizamos lo siguiente:
  * En la opcion __Run__ de IntelliJ buscamos la opcion __Edit configurations__ 
  * En la pantalla que despliega buscamos el campo __Program arguments__..
  * En este campo asignamos los valores para los args de la funcion.

## Programación Funcional: Funciones Puras e Inmutabilidad

* Programar de manera funcional significa que lo haremos de forma declarativa, es decir nos preocuparemos más por el qué que por el cómo.

* En este paradigma casi todo lo que habíamos aprendido sobre programación dejaremos de usarlo. Nos olvidaremos de usar variables como algo que puede cambiar, en su lugar creeremos en la inmutabilidad. Los bucles for, while, do while, etc. dejarán de existir pues ahora tendremos recursividad, además que, como ya dijimos, las funciones serán tan especiales que estas deberán ser funciones puras por definición.

* [Lectura Interesante Programacion Funcional](https://medium.com/@sho.miyata.1/the-object-oriented-programming-vs-functional-programming-debate-in-a-beginner-friendly-nutshell-24fb6f8625cc)

### Funciones Puras

* Una función pura, deberá cumplir con dos cosas específicamente.

  * __Primero.__ Dados los mismos parámetros de entrada la función debe retornar siempre el mismo valor.

  * __Segundo.__ La función no debe tener efectos colaterales, es decir no debe haber nada en el entorno que la altere. Como por ejemplo, variables globales que fue el ejemplo que vimos en la clase anterior.

### Inmutabilidad

* __La inmutabilidad__ es uno de los principios de la programación funcional donde nos promueve la ausencia de estado mutable o también conocido como Stateless, para entender esto mejor lo primero que debemos comprender es qué cosa se considera estado.

* __Estado:__ será cualquier dato que se pueda guardar y modificar posteriormente en memoria

## Strings

* Kotlin es un lenguaje en el cual podemos declarar variables con tipado estricto(declarando si es String, Int, Float, etc) o no tipado.

* Una de las caracteristicas de la Programacion Funcional es declarar las variables con un tipado estricto.

* Caracteres especiales: \t, \b, \n, \r, \', \", \\ y \$

## Conversion de tipos de datos

* String a Int e Int a String
* Long a Int e Int a Long
* String to Int, and Int to String Conversion
* Double a Int e Int a Double
* Long a Double y Double a Long
* Char a Int e Int a Char
* String a Long y Long a String
* String a Array y Array a String
* String a Boolean y Boolean a String
* String a Byte y Byte a String
* Int a Byte y Byte a Int

## Rangos

* En kotlin los rangos se declaran de una manera mas sencilla mediante el operador '..'.
* Se puede utilizar una variable para almacenar el rango y luego recorrerla mediante un ciclo for o recorrerla directamente en el ciclo for.
* Como buena practica de la programacion funcional, se declara el tipo de variable.

## If y When en Kotlin

* __Sentencias de control__ permiten controlar el flujo del codigo.
* __For, If, Whe, While, Do While__ son sentencia de control.
* __If__ es un operador logico, devuelve true o false.
* __When__ es igual a switch en otros lenguales y siempre viene acompañado rangos
  * Utilizamos __in__ para ir evaluando las opciones mediante rangos
  * Utilizamos __->__ para hacer algo
  * __else__ es una opcion por default en caso de que ninguno de los __in__ se utilicen

## Bucles while y do while

* Se entiende como una combinacion de usar __for__ y usar __if__ , ya que evalua una sentencia y devuelve un true o false
* Por lo general se usa con contandores para verificar que se cumpla una condicion
* En el caso de __do while__ 
  * En el __do__ primero ejecuta la accion __println__
  * Luego en el __while__ evalua la condicion
  * Es decir aqui siempre, como nimino, se ejcutara una vez la acción

## Ciclos for y foreach en Kotlin
* El ciclo __for__ su sintaxis es mas larga a compaaracion de __foreach__
* En ambos ciclos podemos obtener los indices de los elementos en la lista
* En el caso de __foreach__ usaramos la palabra __it__ como iterador, el cual contendra cada elemento de la lista recorrida
* Ambas formas de recorrer listas parecen ser igual, sin embargo se ha comprobado que un ciclo for es 10x más rápido que uno foreach

## Break, Continue y Labels
* __Break__ termina el ciclo mas cercano
* __Continue__ va a la siguiente linea de codigo del ciclo mas cercano
* __Labels__ podemos usar labels para controlar mejor los saltos y definir en qué ciclo queremos que inicie después de saltar.
Un label será un nombre que fungirá como identificador clave para el punto y/o ciclo específico al cual deseamos saltar, lo usaremos con break y continue llamándolo por el mismo nombre definido.
* La sintaxis de un __label__ es __*nombre@*__

## Valores Nulos y Double Bang
* __Valor Nulo__ es el pariente mas cercano a la nada
* Una buena practica de programacion es comenzar con variables __no nulas__
* Kotlin es __Null Safety__ evita caer en indeterminaciones que provoquen *NullPointerException*
* Kotlin evita que una excepcion sea lanzada por que provoca vulnerabilidades
* En Kotlin ningun valor de una variable puede ser nulo
* A veces la necesidad orilla a definir valores nulos, para eso usamos el signo __?__ al declarar el tipo de varible. Ejemplo:
  * var __algo:String?__ = null
* __Double Bang__ operador que nos permite manejar una excepcion. En caso de que un operador sea nulo con este operador vamos a forzar o lanzar una validacion *try/catch* para que se muestre la excepcion.
* Si usamos el opeador de __double bang__ la excepcion *__siempre__* tiene que ser manejada mediante la validacion

## Operador Elvis
* __?:__ Operador Elvis
* Es parecido a un operador ternario en JS
* Se utiliza para comparar si los valores son iguales a nulo y si no. Ejemplo
* listFiles()?.size ?: 0
  * Si la lista es diferente de null, entonces llama al metodo size
  * Y si no devuleve 0
  
## ¿Que es un Array en Kotlin?
* __Arrays o arreglos__ son colecciones de datos que nos permiten almacenar grandes o multiples cantidades de informacion en una sola variable.
* Podemos declararlos de difertenes maneras:
  * __arrayOf__ en el cual no delcaramos el tipo de dato que contendra el arreglo
  * __arrayOf< String >__ de esta manera le pasamos el tipo de dato que contendra el arreglo
  * __intArrayOf__ en esta opcion desde la declaracion del array definimos el tipo de dato que contendra, utilizamos esta opcion para ejemplificar la manera en que se puede declarar. Existen mas opciones __boolean, String, Float...etc.

## Métodos utiles en arreglos
* __arrayOf__ esta preparado para manejar tipo de datos objeto
  * Los tipos de datos objeto tienen metodos para manejar operaciones mucho mas simple
* __intArrayOf__ esta preparado para manejar tipo de datos primitvo
  * Los tipos de datos primitivos nos ayudan a manejar operaciones mucho mas siumples, mucho mas visuables. Estos tambien tienen algunos metodos para hacer operaciones mas simples.
* Habra ciertos metodos que si esten en los datos tipo objeto pero no esten en los primitivos.

## Expresiones vs Valores
* __Expresiones__ conjunto de variables y operadores que devuelven un valor
* En kotlin las __expresiones__ juegan un papel muy importante, es una de las claves para hacerlo mas conciso
* Todo es una expresion en Kotlin
* En kotlin siempre se devuelve un valor, aun si el valor es nada
* __$__ pasar un valor
* __${}__ pasa una expresion

## Funciones en Kotlin
* En kotlin hay dos tipos de __funciones__:
  * Funciones provistas por Kotlin
  * Funciones declaradas por nosotros
* En una __funcion__ siempre se devuelve un valor, aunque este sea igual a nada
* En el caso de __Math.sqrt()__ se interpreta de la siguiente manera:
  * __Math__ es la clase que contiene todas las funciones
  * __sqrt()__ es un funcion de la clase Math y dentro de los parentesis pasaremos el valor, si asi lo requiere
* En una funcion propia, Kotlin tiene una peculiaridad, se debe devolver el valor de retorno
* Si la funcion no devuelve nada, existe un tipo de valor para declarar esa nada __Unit__
* Las __funciones propias__ van fuera de la funcion main y se llaman dentro de esta
* Se pueden pasar varios parametros, definidos en la funcion y seaprados por comas

## Funciones con parámetros por defecto
* Como su nombre lo indica, en algunos casos las funciones ya tendran parametros por defecto
* En los parametros declararemos el valor de los que queramos que sean por defecto. Ejemplo __fun evaluate__:
  * character: Char = '=' , number: Int = 2 *Aqui tenemos dos parametros declarados por defectos*
* Al llamar a la funcion evaluate sin pasarle parametros, esta usara los que ya estan declarados por defecto
* Si quisieramos cambiar el valor de los paramteros, podriamos hacerlo al momento de llamar la funcion
* No se puede cambiar el parametro 2 dejando vacio el 1, para este caso lo mejor sera cambiar el orden de los parametros en la funcion

## Lambdas en Kotlin
* Son funciones __anonimas__, que no tiene nombre
* En el archivo HolaMundo.kt se ven ejemplos de algunas maneras de utilizar la lambda


# Programación Orientada a Objetos

## Clases
* Una clase es en mi manera de entender, la manera en que creamos un objeto con atributos y metodos.
* Para declara una clase en Kotlin usamos la palabra reservada __Class__
* Puede tener propiedades __var color, var altura, etc__
* Entonces la clase se compone de:
  * El __nombre(titulo)__ de la clase 
  * Sus __propiedades(atributos)__
  * Las __funcionalidades(metodos)__

## Ejercicio de clases
* Principio de la POO es que debemos separa nuestro codigo cuanto podamos.
* Por eso crearemos nuestra clase en un archivo aparte
* Las clases siempre deben llevar el mismo nombre del archivo
* Deben empezar siempre con mayusucula para poder diferenciarla de una variable, las cuales empiezan con minuscula
* __OJO__ *Ningun atributo debe ser alterado por alguien a externo a la misma clase, como lo hicimos en el ejemplo esta mal. Mas adelante explicara como modificarlos de la manera correcta*


## Modificadores de acceso
* En kotlin todos los valores, atributos, variables, etc son publicos por defecto
* De nosotros depende decidir cuales valores seguiran publicos o cuales deben ser privados
* Una forma de solucionarlos es modificando los modificadores de acceso que son:
  * __Public__ todo acceso. Por defecto todos los valores..etc tienen este modificador
  * __Private__ acceso solo dentro de la clase
  * __Protected__ accesos solo dentro de la case y las clases que hereden
  * __Internal__ acceso entre modulos
* La manera en que usamos los modificadores de acceso es la siguiente forma:
  * __private var algo__
  * A este tipo de declaracion se le conoce como __encapsulamiento__
  * Limitamos el acceso a la variable directamente, pero dejamos metodos para saber acerca de ella

## Getters y Setters
* Kotlin tiene dos formas de hacer los Getters y Setters
  * Provisto por el programador
  * Acceso directo desde Kotlin
* Algunos metodos Get y Set podrian no existir dada la definicion de otros metodos que ya hacen ese ejercicio
* Con __this__ nos referimos a todos los elementos(variables, metodos) de nuestra clase 

## El Get() y Set() propio de Kotlin
* Una diferencia es que en las clases de estos metodos get y set las variables son publicas
* El tipo de clase que creamos cuando utilizamos los metodos set y get propio de kotlin se le conoce como __Clases para datos(Data Class)__
* En este tipo de clases el metodo set y get se utiliza por lo general cuando traemos datos de internet mediante una API
* __field__ es un tipo de variable que toma temporalmente el valor de la variable(en el ejemplo var size) y devuelv el valor
* No utilizamos el metodo get, en este ejemplo, ya que seria redundante por que en el mismo metodo set especificamos el valor que tendra como vase la variable size y mediante las condiciones if sabremos si debe cambiar o quedarse con el valor base

## Data Class en Kotlin
* Los Data Class son tipos de clases que nos ayudan a manejar datos como tal
* Estos datos podrian vivir en otra parte que no precisamente sea nuestro codigo(base de datos, api, cualquier otra fuente de datos)
* Utilizamos los data class para mapear esa fuente datos y asi poder manejar la consistencia de atributos de los datos
* Lad data class llevan antes de la declaracion de la clase el tipo de clase ene ste caso __data__
* Las data class pueden vivir en el mismo archivo donde esta la funcion main, pero la definicion de la clase no sera clara
* Lo mejor es que este en otro archivo y asi seguimos el concepto de modularidad en el codigo de la POO

## Metodo Constructor
* __Constructor__ es aquel metodo que nos permite construir un objeto
* Cuando a una clase le ponemos parentesis, Kotlin entiende que es un metodo para construir un objeto
* Tenmemos dos tipos de constructores:
  * __Constructor Primario__ Forma clasica de inicializar una clase, por ejemplo al agregar parentesis a una clase para pasar argumentos, ese parentesis es un constructor primario.
  * __Constructor Secunadario__ Cuando ponemos una inicializacion, es decir ponemos un poco mas de codigo para inicializar esa clase. Se utiliza palabra reservada init

## Herencia y polimorfismo
* __Herencia (*es un pilar de la POO*)__ sirve para crear nuevas clases apartir de otras. Reutilizar codigo.
  * Se establece una relación __padre e hijo__ donde la clase padre sera la __Superclase__. Cuando veamos en el codigo la palabre __Super__ es a esta Superclase a la que se refiere.
  * Y las clase hijos sera __Subclase__
  * __Any__ es la clase padre, *Superclase* de todas las clases que queramos crear
  * La sintaxis para heredar es __*:superclase()*__ esto va despues del metodo constructor de la clase
* __Polimorfismo__ Poli = Muchas, Morfismo = Formas
  * Dar un comportamiento distinto a un metodo heredado de una clase padre
  * Siempre que sobreescribamos un metodo estaremos usando *polimorfimos* y este siempre de la mano con la *herencia*

## Aplicando herencia y polimorfismo
* En kotlin __no se puede heredar de una clase__ ya que estas por defecto estan cerradas
* Para poder heredar de una clase cualquiera es necesario que al delcarar la clase se utilize la siguiente sintaxis __*open class Algo*__. 
* La palabra *open* abre las clases y funciones, asi podemos acceder a ella
* No es necesario heredar la clase padre Any ya que por default todas las clases heredan de esta
* Una clase no puede heredar de dos padres. Es por eso que en el archivo Shoe.kt quitamos la herencia de Any y heredamos de Product

## Clases Abstractas
* __Clases abstractas__ son clases que no tienen implementacion. Hacer una clase mas generica de la clase padre. 
* __*No podemos crear instancias de ellas, es decir no podemos hacer objetos.*__
* Obligatoriamente todas las __clases__ y __metodos__ abstract deben ser *heredados*
* Metodos abstractos sin declaracion y metodos declarados pueden convivir
* Tienen metodos declarados pero sin implementacion
* Se declaran metodos sin implementacion(cuerpo) para que al momento de ser heredados se pueden implentar de la manera que mejor corresponda
  * Esto es una de las *buenaa practica* de programacion, dejar codigo reutilizable por cualquier otra persona

## Interfaces
* Muy similares a las *clases abstractas*
* No son declaradas con la palabra reservada *class* sin embargo se consideran un tipo de clase
* Estan enfocadas a las __acciones, funcionalidades, metodos__ que resulten redudantes entre clases
  * Estas podran aplicarse a cualquier nivel de la jerarquia de clases ya sean padres o hijos
* Cuando encontramos __atributos o propiedades__ redundantes entre las clases es cuando usamos *clases abstractas*
  * Se delegan hacia abajo en la jerarquia de clases, es decir solo a la case hijo y siempre tendremos que estar aplicando herencia
* Las interfaces se nombran con la primera letra sea una __I__ o con la terminacion __able__ 
* Podemos importar todas las interfaces que necesitemos usar
* En las interfaces podemos usar metodos con implementacion y sin implementacion, deben ser lo mas generico posibles

# Programación Funcional

## Funciones de orden superior
* Funciones que reciben como parametro a otras funciones.
  * Son aquellas que van dentro de los parametros de otra funcion y reutilizan las variables que se pasan ahí mismo. Ejemplo: __fun funcion(variables, ::otraFuncion)__ Aqui *otraFuncion* utilizara las *variables* que se pasan como parametros

## Una funcion como paramtero
* En este captiulo vimos como se utlizan las funciones de orden superior pasandole algunos parametros
* Las funciones de orden superior pueden recibir como parametro una lambda (n -> n), explicitamente.
* Las funciones de orden superior llaman a las otras funciones mediante los __:: nombre funcion__
* Ejemplo en el archivo HolaMundo.kt
* Tambien pueden recibir *callbacks* esto por lo general es cuando consultamos una API