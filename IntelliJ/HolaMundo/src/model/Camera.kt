package model

class Camera {

    private var isOn:Boolean = false
    private var resolution:Int = 640
    private var flash:Boolean = false

    // Ejemplos de getters y setter provistos
    // por el programador con resolution
    fun setResolution(resolution:Int){
        this.resolution = resolution
    }

    fun getResolution():Int{
        return this.resolution
    }

    fun setFlash(flash:Boolean){
        this.flash = flash
    }

    fun getFlash():Boolean{
        return this.flash
    }

    fun turnOn() {
        isOn = true
    }

    fun turnOff() {
        isOn = false
    }
// Funcion tipo get provista por el programador
    fun getCameraStatus(): String {
        return if(isOn) "Camera is turned" else "Camera is not turned"
    }
// Esta funcion es un tipo Set provista por el programador pero es
// redundante con las otras funciones turOn y turnOff
//    fun setCameraStatus(onOff: Boolean){
//        isOn = onOff
//    }
}