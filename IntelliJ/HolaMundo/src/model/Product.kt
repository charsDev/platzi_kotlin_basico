package model

// Para hacer una clase abstracta tenemos que ponerle abstract en lugar de open
abstract class Product(var name:String, var description:String, var sku:Int){

    override fun toString(): String {
        return "\nName: $name \nDescription: $description \nSKU: $sku"
    }
}