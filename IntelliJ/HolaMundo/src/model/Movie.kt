package model

// Esta si es una Data Class
data class Movie(val title:String, val creator:String, val duration:Int)