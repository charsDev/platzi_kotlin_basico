package model


// Los parntesis en la clase son el Constructor Primario
class Shoe(sku:Int, var brand: String, name:String, description:String):
    // Heredamos de la clase Product, como las variables ya estan declaradas en el constructor de la
    // clase Padre, solo las llamamos y tenemos que declarar el tipo de dato en el constructor de la
    // clase hijo en este caso Shoe, como podemos ver en este ejemplo.
    // Como podemos mark sigue con su declaracion de tipo de variable,
    // ya que esta no viene de la clase padre
    Product(name, description, sku), ICrudActions{ // Se debe poner una coma para poder llamar a la interfaz

    override fun create():String{
        saludar("Hola desde create interface")
        return "Create shoe"
    }


    override fun read(): String {
        return "Read shoe"
    }

    override fun update(): String {
        return "Update shoe"
    }

    override fun delete(): String {
        return "Delete shoe"
    }

    // Esto es polimorfismo ya que estamos cambiando una funcion de la clase padre Any
    // esta clase la llamamos por herencia al iniciar la clase despues del metodo constructor
    // de la siguiente forma :Any()
    override fun toString(): String {
        return super.toString() + "\nSKU: $sku \nMarca: $brand \nModelo: $brand \nSize: $size \nColor: $color"
    }


    // Constructor Secundario
    init {
        println("SKU ID: $sku")
        println("Brand: $brand")
    }

    var size:Int = 34 //Minimo sea 34
        set(value) {
            if(value >= 34)
                field = value
            else
                field = 34
        }
    var color:String = "white"
    var model: String = "Boots" //No se ponga tenis
        set(value) {
            if(value.equals("Tenis"))
                field = "Boots"
            else
                field = value
        }
}