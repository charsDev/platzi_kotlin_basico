import model.Camera
import model.Movie
import model.Shoe
import java.lang.Math.random

const val N = "NAME" // const tiempo de compilacion
var n = "n" // mala practica de programacion

fun main(args: Array<String>) {
    println("Hola Mundo")

    // Operadores con tipo de datos primitivos
    println(1+1)
    println(3-1)
    println(2*2)
    println(4/2)
    println(7%2)

    // Tratando valores primitvos como objetos
    val a = 4
    val b = 2

    println(a.plus(b))
    println(a.minus(b))
    println(a.times(b))
    println(a.div(b))

    // Ejercicio var val const
    val name = args[0] //tiempo de ejecucion
    println(name)

    println(N)

    // Strings
    val nombre = "Carlos" // variable no tipada
    val apellido: String = "Garcia" // variable tipado estricto

    println("Tu nombre es: $nombre $apellido" ) // concatenacion de strings al imprimir

    val nombreapellido = "Carlos\nCastellanos" // caracteres especiales: \t, \b, \n, \r, \', \", \\ y \$

    println("Tu nombre es: $nombreapellido") // Imprime el nombreapellido con salto de linea

    val parrafo:String = """
        ** Lorem ipsum dolor sit amet, consectetur adipiscing elit,
        ** sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        ** Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
        ** nisi ut aliquip ex ea
        ** commodo consequat.
    """.trimIndent() // Formatear parrafos con trimIndent. Metodo de los Strings

    println(parrafo.trimMargin(marginPrefix = "** ")) // Formatear un parrafo eliminado caracteres

    // Rangos
    val oneToHundred:IntRange = 1..5 // Declaramos variable con el rango deseado

    for (i:Int in oneToHundred){ //mediante un for recorremos el rango y lo imprimimos
        println(i)
    }

    // val aToC:CharRange = 'A'..'C' <--- no utilizamos la variable con el rango delcarado
    for (letra:Char in 'A'..'C'){
        println(letra)
    }

    // If y When en Kotlin
    val numero = 20
    if (numero.equals(2)) {
        println("Si son iguales")
    }else{
        println("No son iguales")
    }

    when(numero){
        in 1..5 -> println("Si esta entre 1 y 5")
        in 1..3 -> println("Si esta entre 1 y 3")
        //!in 5..10 -> println("No no esta entre 5 y 19")
        else -> println("No esta en ninguno de los anteriores")
    }

    // Bucles While y Do While
    var i = 1

    while (i<1){
        println("mensaje: $i")
        i++
    }

    i = 1
    do {
        println("mensaje do while: $i")
        i++
    } while (i<1)

    //Ciclos for y foreach
    var daysOfWeek = listOf("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado")
    //for
    for(day in daysOfWeek){
        println(day)
    }
    // acceso a indices
    for((index,day) in daysOfWeek.withIndex()){
        println("$index :$day")
    }
    // foreach
    daysOfWeek.forEach{
        println(it)
    }

    // Breaak, Continue y Labels
    // break
    println("--- Break ---")
    for (i in 1..3) {
        println("\ni: $i ")
        for (j in 1..5) {
            if (j.equals(3)) break
            println("j: $j")
        }
    }
    // continue
    println("--- Continue ---")
    for (i in 1..3) {
        println("\ni: $i ")
        for (j in 1..5) {
            if (j.equals(3)) continue
            println("j: $j")
        }
    }
    // break y label
    println("--- Break y Label ---")
    terminarTodoCiclo@ for (i in 1..3) {
        println("\ni: $i ")
        for (j in 1..3) {
            println("\nj: $j")
            for (k in 1..5) {
                if (k.equals(3)) break@terminarTodoCiclo
                println("k: $k")
            }
        }
    }
    // continue y label
    println("--- Continue y Label ---")
    escaparJ@ for (i in 1..3) {
        println("\ni: $i ")
        for (j in 1..3) {
            println("\nj: $j")
            for (k in 1..5) {
                if (k.equals(3)) continue@escaparJ
                println("k: $k")
            }
        }
    }

    // Ejercicios con Null Safety y operador Elvis
    println("\nEjercicios Null Safety y operador Elvis")

    try{
        var compute: String?
        compute = null
        var longitud: Int = compute!!.length
    }catch(e: NullPointerException){
        println("Ingrea un valor, no aceptamos nulos")
    }

    // Llamada Segura evita hacer el try/catch
    var compute: String? = null
    var longitud: Int? = compute?.length
    println("Longitud: $longitud")

    //Operador Elvis
    var teclado: String? = null
    val longitudTeclado: Int = teclado?.length ?: 0
    /* Si la longitud de teclado no es nula, entonces el valor de length,
    si no devuelve valor 0*/
    println("Longitud del teclado: $longitudTeclado")

    val listWithNulls: List<Int?> = listOf<Int?>(7, null, null, 4)
    println("Lista con Null: ${listWithNulls}")
    // Usamos el metodo filterNotNull()
    val listWithoutNulls: List<Int?> = listWithNulls.filterNotNull()
    println("Lista sin Null: ${listWithoutNulls}")

    // Array en Kotlin
    println("\nArreglos")
    val countries = arrayOf("India", "México", "Colombia", "Argentina", "Peru", 1, 2)

    val days = arrayOf<String>("Lunes", "Martes", "Miercoles")

    //Promedio de los numeros
    val numbers = intArrayOf(6, 6, 23, 9, 2, 3, 2)
    var sum = 0
    for (num in numbers){
        sum += num
    }
    var average = sum / numbers.size
    println("Promedio: $average")

    // Metodos utiles en arreglos
    println("\nMetodos utiles en arreglos")

    var arrayObject = arrayOf(1,2,3)

    //convirtiendo de objecto a primitvo
    var intPrimitive: IntArray = arrayObject.toIntArray()

    val suma = arrayObject.sum()
    println("La suma del array es: $suma")

    arrayObject = arrayObject.plus(4)
    for (a in arrayObject){
        println("Array $a")
    }
    /*El metodo reverse no necestio almacenarse en una varibale como si
    sucede en el metodo reversedArray*/
    /*arrayObject.reverse()
    for (a in arrayObject){
        println("Reverse unit: $a")
    }*/

    arrayObject = arrayObject.reversedArray()
    for (a in arrayObject){
        println("Array reversa: $a")
    }

    // Expresiones vs Valores
    println("Expresiones vs Valores")

    // expresiones
    var x = 5
    println("¿X es igual a 5? ${x==5}") //forma simple de expresiones

    var mensaje = "El valor de x es $x"
    x++
    println("${mensaje.replace("es", "fue")}, x es igual a: $x") //forma mas compleja de expresion

    // Funciones Kotlin
    println("\nFunciones")

    println("Raiz cuadrada de: ${Math.sqrt(4.0)}") // Ejemplo Funcion previista por Kotlin

    val numeros = intArrayOf(6, 6, 23, 9, 2, 3, 2)
    println("El promedio de los numeros es: ${averageNumber(numeros, 2)}")

    println("${evaluate('+',2 )}")

    // Lamdbas en Kotlin
    println("\nLambdas")

    val saludo = { println("Hola Mundo Lambda")} // Lamda sin parametros
    saludo()

    // Lambda con parametros
    val w = {d: Int, c: Int -> d+c}
    println(w(2,3))

    // Version mas corta
    println({d: Int, c: Int -> d+c}(4,4))

    // Ejemplos de lambdas
    val hola = { println("Hello")}
    hola()

    // var suma = {instrucciones -> sentencias}
    val plus ={a:Int, b:Int, c:Int -> a+b+c}
    val result = plus(3,4,5)

    println(result) // manera normal, muchos pasos, muy explicita

    println(plus(7,8,9)) // manera corta, se entiende

    println({a:Int, b:Int, c:Int -> a+b+c}(1,4,6)) // manera aun mas corta, pero menos explicita

    // Otro ejemplo para usar lambdas un poco mas elaborado
    val calculateNumber = {n:Int ->
        when(n){
            in 1..3 -> println("Tu numero esta entre 1 y 3")
            in 4..7 -> println("Tu numero esta entre 4 y 7")
            in 8..10 -> println("Tu numero esta entre 8 y 10")
        }
    }
    println(calculateNumber(2))

    // Ejercicio de Clases
    println("\nEjercico de Clases")

    val camera = Camera() // declaramos un objeto

    //encedemos la camara
    camera.turnOn() // accediendo a un metodo de la clase

//    println("La camara esta: ${camera.isOn}") //accediendo a un atributo de la clase

    // Modificadores de acceso
    println("\nModificadores de acceso")
    // Debido a que no se debe tener acceso a las variables de manera publica
    // en la clase Camera restringimos una variable con Private y creamos una funcion
    // la cual nos dira si la clase esta prendida o no
    println(camera.getCameraStatus())

    camera.setResolution(1080)
    println("Resolution ${camera.getResolution()}")

    camera.setFlash(true)
    println("El flash esta: ${camera.getFlash()}")

    // Get y Set propio de Kotlin
    println("\nGet y Set propio de Kotlin")

    // Set propio de Kotlin
    var shoe = Shoe(123445, "Praga", "Shoe", "blue shoes") // Creamos un objeto shoe
    println(shoe)
    shoe.create() // Llamamos al metodo create, este contiene la funcion saludar que esta en la interfaz

    // Herencia y Polimorfismo
    // aqui mandamos a imprimir el objeto shoe y como no llamamos atributos ni metodos nos mostrara
    // la superclase que cambiamos con polimorfismo para que imprima "Polimorfismo"
    println("Shoe: ${shoe}")
    // Set propio de Kotlin
//    shoe.size = 38 // si el valor es mayor que 34 le asgina el valor dado, pero si no lo deja en 34
    // Get propio de kotlin
//    println(shoe.size)
//
//    shoe.model = "Tenis"
//    println(shoe.model)

    // Constructor Primario
//    print(shoe.mark)

    // Data class en Kotlin
    println("\nData Class")

    val movie = Movie("Coco", "Pixar", 120)
    println(movie.title)
    println(movie.creator)
    println(movie.duration)

    val auto = Auto("Nissan", 2019, "Rojo")
    println(auto.marca)
    println(auto.modelo)
    println(auto.color)

    // Una funcion como parametro - Funciones de orden superior
    println("\nUna funcion como parametro - Funciones de orden superior")

    val resultado_multiplicacion = calculadora(1,2,3, ::multiplicar) //asi es como usamos las funciones de orden superior
    println("El resultado de la multiplicacion es $resultado_multiplicacion")

    println("El resultado de la resta es ${calculadora(4,5,6, :: restar)}")
    println("El resultado de la suma es ${calculadora(4,5,6, :: sumar)}")

}
// Estructuramos la funcion de orden superior
fun calculadora(a:Int, b:Int, c: Int, operacion:(Int, Int, Int)-> Int):Int{

    return operacion(a,b,c)
}
// Creamos las funciones que seran las operaciones que utilizara la lambda de nuestra funcion de orden superior
fun sumar(a: Int, b: Int, c: Int) = a+b+c
fun restar(a: Int, b: Int, c: Int) = a-b-c
fun multiplicar(a: Int, b: Int, c: Int) = a*b*c

data class Auto(val marca:String, val modelo:Int, val color:String)

fun evaluate(character: Char = '=', number: Int = 2): String{
    return "${number} es ${character}"
}

fun averageNumber(numbers: IntArray, n: Int): Int{
    //Promedio de los numeros
    var sum = 0
    for (num in numbers){
        sum += num
    }

    return (sum/numbers.size)+n
}