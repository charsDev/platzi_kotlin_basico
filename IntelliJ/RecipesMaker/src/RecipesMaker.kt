import java.lang.NumberFormatException

fun main () {
    val menu: String = """
        :: Bienvenido a Recipe Maker ::


        Selecciona la opción deseada
        1. Hacer una receta
        2. Ver mis recetas
        3. Salir
    """.trimIndent()
//    println(menu)

    try{
        println(menu)
        do{
            val response: String? = readLine()
            val responseToInt: Int? = response!!.toInt()
            when(responseToInt) {
                1 -> makeRecipe()
                2 -> viewRecipe()
                3 -> println("Salir")
                else -> println("Opcion no valida. \nVuelve a intentarlo")
            }
        }while (responseToInt!! < 3)
    }catch(e: NumberFormatException){
        println("La opcion debe ser un numero. \nVuelve a intentarlo")
    }
}

fun makeRecipe() {
    val ingredientes: String = """
        Hacer receta
        Selecciona por categoría el ingrediente que buscas
        1. Agua
        2. Leche
        3. Carne
        4. Verduras
        5. Frutas
        6. Cereal
        7. Huevos
        8. Aceites
    """.trimIndent()
    println(ingredientes)
    val response: String? = readLine()
    if (response?.toInt() == 5) {
        for ((index, fruta) in frutas.withIndex()){
            println("${index+1}. $fruta")
        }
    }
}

fun viewRecipe(){
    println("Ver mis recetas")
}

val frutas = arrayOf("Fresa", "Plantano", "Uvas", "Manzana", "Naranja", "Pera", "Cereza")
